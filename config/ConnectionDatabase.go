package config

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

var (
	client *mongo.Client
	err    error
)

type ConnectionDatabase struct {

}

func (c *ConnectionDatabase) ConnectionDatabase() (*mongo.Client, context.Context) {
	client, err = mongo.
		NewClient(options.Client().ApplyURI("mongodb+srv://admin:tham5497@javagraphql.ktshy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 20*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, ctx
}

