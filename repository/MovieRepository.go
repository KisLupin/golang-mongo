package repository

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/KisLupin/golang-mongo/config"
	"gitlab.com/KisLupin/golang-mongo/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"net/http"
	"strconv"
)

type MovieRepository struct {
}

type MovieService interface {
	FindById(r *http.Request) (*model.Movie, error)
	FindByField(w http.ResponseWriter, r *http.Request) []*model.Movie
	Add(r *http.Request) *mongo.InsertOneResult
	DeletePost(w http.ResponseWriter, id primitive.ObjectID) *mongo.DeleteResult
	UpdatePost(r *http.Request) *model.Movie
}

func (m *MovieRepository) FindById(r *http.Request) (movie *model.Movie, err error) {
	connectionDB := config.ConnectionDatabase{}
	client, ctx := connectionDB.ConnectionDatabase()
	defer client.Disconnect(ctx)
	params := mux.Vars(r)
	_id, _ := primitive.ObjectIDFromHex(params["id"])

	collection := client.Database("sample_mflix").Collection("movies")
	err = collection.FindOne(ctx, bson.M{"_id": _id}).Decode(&movie)
	return movie, err
}

func (m *MovieRepository) FindByField(w http.ResponseWriter, r *http.Request) (movies []*model.Movie) {
	connectionDB := config.ConnectionDatabase{}
	client, ctx := connectionDB.ConnectionDatabase()
	defer client.Disconnect(ctx)
	var body map[string]string
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return
	}
	field := body["field"]
	value := body["value"]
	operator := body["operator"]
	intValue, errInt := strconv.Atoi(value)
	collection := client.Database("sample_mflix").Collection("movies")
	var cur *mongo.Cursor
	if errInt == nil {
		cur, err = collection.Find(ctx, bson.M{field: bson.M{operator: intValue}})
	} else {
		cur, err = collection.Find(ctx, bson.M{field: bson.M{operator: value}})
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
	}
	if err = cur.All(ctx, &movies); err != nil {
		panic(err)
	}
	return movies
}

func (m *MovieRepository) Add(r *http.Request) *mongo.InsertOneResult {
	connectionDB := config.ConnectionDatabase{}
	client, ctx := connectionDB.ConnectionDatabase()
	defer client.Disconnect(ctx)
	var movie model.Movie
	json.NewDecoder(r.Body).Decode(&movie)
	collection := client.Database("sample_mflix").Collection("movies")
	result, err := collection.InsertOne(ctx, movie)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (m *MovieRepository) DeletePost(w http.ResponseWriter, id primitive.ObjectID) *mongo.DeleteResult {
	connectionDB := config.ConnectionDatabase{}
	client, ctx := connectionDB.ConnectionDatabase()
	defer client.Disconnect(ctx)
	collection := client.Database("sample_mflix").Collection("movies")
	res, err := collection.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return nil
	}
	return res
}

func (m *MovieRepository) UpdatePost(r *http.Request) (mv *model.Movie) {
	connectionDB := config.ConnectionDatabase{}
	client, ctx := connectionDB.ConnectionDatabase()
	defer client.Disconnect(ctx)
	var movie model.Movie
	json.NewDecoder(r.Body).Decode(&movie)
	collection := client.Database("sample_mflix").Collection("movies")
	_, err := collection.UpdateOne(ctx, bson.M{"_id": movie.ID},
		bson.D{
			{"$set", bson.D{{"title", movie.Title}}},
		},
	)
	if err != nil {
		log.Fatal(err)
	}
	mv = &movie
	return mv
}
