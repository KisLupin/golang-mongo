package controller

import (
	"github.com/gorilla/mux"
	"gitlab.com/KisLupin/golang-mongo/service"
)

type SecurityController struct {
}

func (s *SecurityController) RunAPI(router *mux.Router) {
	securityService := service.SecurityServiceImpl{}
	router.HandleFunc("/jwt", securityService.GetJwt).Methods("GET")
}
