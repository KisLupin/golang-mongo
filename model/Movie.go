package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type IMDB struct {
	Rating float64 `bson:"rating,omitempty" json:"rating"`
	Votes  int     `bson:"votes,omitempty" json:"votes"`
	Id     int     `bson:"id,omitempty" json:"id"`
}

type Viewer struct {
	Rating     float64 `bson:"rating,omitempty" json:"rating"`
	NumReviews int     `bson:"numReviews,omitempty" json:"numReviews"`
	Meter      int     `bson:"meter,omitempty" json:"meter"`
}

type Critic struct {
	Rating     float64 `bson:"rating,omitempty" json:"rating"`
	NumReviews int     `bson:"numReviews,omitempty" json:"numReviews"`
	Meter      int     `bson:"meter,omitempty" json:"meter"`
}

type Tomato struct {
	Viewer      Viewer             `bson:"viewer,omitempty" json:"viewer"`
	Dvd         primitive.DateTime `bson:"dvd,omitempty" json:"dvd"`
	Critic      Critic             `bson:"critic,omitempty" json:"critic"`
	LastUpdated primitive.DateTime `bson:"lastUpdated,omitempty" json:"lastupdated"`
	Rotten      int                `bson:"rotten,omitempty" json:"rotten"`
	Production  string             `bson:"production,omitempty" json:"production"`
	Fresh       int                `bson:"fresh,omitempty" json:"fresh"`
}

type Movie struct {
	ID                 primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	PLot               string             `bson:"plot,omitempty" json:"plot"`
	Genres             []string           `bson:"genres,omitempty" json:"genres"`
	Runtime            int                `bson:"runtime,omitempty" json:"runtime"`
	Rated              string             `bson:"rated,omitempty" json:"rated"`
	Cast               []string           `bson:"cast,omitempty" json:"cast"`
	Num_mflix_comments int                `bson:"num_mflix_comments,omitempty" json:"num_mflix_comments"`
	Poster             string             `bson:"poster,omitempty" json:"poster"`
	Title              string             `bson:"title,omitempty" json:"title"`
	LastUpdated        string             `bson:"lastUpdated,omitempty" json:"lastupdated"`
	Languages          []string           `bson:"languages,omitempty" json:"languages"`
	Released           primitive.DateTime `bson:"released,omitempty" json:"released"`
	Directors          []string           `bson:"directors,omitempty" json:"directors"`
	Writers            []string           `bson:"writers,omitempty" json:"writers"`
	Year               int                `bson:"year,omitempty" json:"year"`
	Imdb               IMDB               `bson:"imdb,omitempty" json:"imdb"`
	Countries          []string           `bson:"countries,omitempty" json:"countries"`
	Types              string             `bson:"types,omitempty" json:"types"`
	Tomatoes           Tomato             `bson:"tomatoes,omitempty" json:"tomatoes"`
	Award              Award              `bson:"awards,omitempty" json:"award"`
}

type Award struct {
	Wins        int    `bson:"wins,omitempty" json:"wins"`
	Nominations int    `bson:"nominations,omitempty" json:"nominations"`
	Text        string `bson:"text,omitempty" json:"text"`
}
