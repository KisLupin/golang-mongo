package service

import (
	"encoding/json"
	"gitlab.com/KisLupin/golang-mongo/config"
	"log"
	"net/http"
)

type SecurityServiceImpl struct {
}

func (s *SecurityServiceImpl) GetJwt(w http.ResponseWriter, _ *http.Request) {
	claims := config.JwtCustomClaims{}
	token := claims.GenerateToken()
	json.NewEncoder(w).Encode(token)
	log.Printf("Generate JWT")
}

func Validate(w http.ResponseWriter, r *http.Request) bool {
	claims := config.JwtCustomClaims{}
	if token, ok := r.Header["Token"]; ok {
		_, err := claims.ValidateToken(token[0])
		if err != nil {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
			log.Printf("JWT is invalid")
			return false
		}
		log.Printf("Validate JWT")
		return true
	}else {
		w.WriteHeader(http.StatusForbidden)
		return ok
	}
}
