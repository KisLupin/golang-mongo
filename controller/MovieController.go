package controller

import (
	"github.com/gorilla/mux"
	"gitlab.com/KisLupin/golang-mongo/service"
)

type MovieController struct {
}

func (c *MovieController) RunAPI(router *mux.Router) {
	movieService := service.MovieService{}
	router.HandleFunc("/movie/{id}", movieService.MovieFindById).Methods("GET")
	router.HandleFunc("/movie/findByCondition", movieService.MoviesFindByField).Methods("POST")
	router.HandleFunc("/movie/add", movieService.Add).Methods("POST")
	router.HandleFunc("/movie/update", movieService.MovieUpdate).Methods("PUT")
	router.HandleFunc("/movie/{id}", movieService.MovieDelete).Methods("DELETE")
}
