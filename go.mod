module gitlab.com/KisLupin/golang-mongo

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	go.mongodb.org/mongo-driver v1.7.1
)
