package service

import (
	"encoding/json"
	"github.com/gorilla/mux"

	"gitlab.com/KisLupin/golang-mongo/repository"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
)

type MovieService struct {
}

func (m *MovieService) MovieFindById(w http.ResponseWriter, r *http.Request) {
	e := Validate(w,r)
	if e != true {
		return
	}
	var movieService repository.MovieService = &repository.MovieRepository{}
	movie, err := movieService.FindById(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(movie)
	log.Printf("FindById")
}

func (m *MovieService) MoviesFindByField(w http.ResponseWriter, r *http.Request) {
	e := Validate(w,r)
	if e != true {
		return
	}
	var movieService repository.MovieService = &repository.MovieRepository{}
	movies := movieService.FindByField(w, r)
	json.NewEncoder(w).Encode(movies)
	log.Printf("FindByField")
}

func (m *MovieService) Add(w http.ResponseWriter, r *http.Request) {
	e := Validate(w,r)
	if e != true {
		return
	}
	var movieService repository.MovieService = &repository.MovieRepository{}
	movie := movieService.Add(r)
	json.NewEncoder(w).Encode(movie)
	log.Printf("Add")
}

func (m *MovieService) MovieUpdate(w http.ResponseWriter, r *http.Request) {
	e := Validate(w,r)
	if e != true {
		return
	}
	var movieService repository.MovieService = &repository.MovieRepository{}
	movie := movieService.UpdatePost(r)
	json.NewEncoder(w).Encode(movie)
	log.Printf("Update")
}

func (m *MovieService) MovieDelete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	var movieService repository.MovieService = &repository.MovieRepository{}
	movie := movieService.DeletePost(w, id)
	json.NewEncoder(w).Encode(movie)
	log.Printf("Delete")
}
